#!/bin/bash
sleep 2s
echo "************************************************************************"
 echo "**                                                                    **"
 echo "**                           MENTA                                    **"
 echo "**                                                                    **"
 echo "************************************************************************"
echo
echo
echo  Saludos!!!
echo
sleep 1s
echo  Este es el Script de Jetix, para instalar el kernel Liquorix Y Xanmod en su ultima version.
echo
sleep 2s
echo  En el proceso, tendrás que poner tu contraseña de usuario.
echo
sleep 3s
while true; do
    echo
    echo  Pero antes de instalar el kernel, Selecciona la version que quieres instalar:
    sleep 3s
    echo
    echo   1:linux-xanmod      Version Estable                      Debian y ubuntu
    
    echo   2:linux-xanmod-edge Version avanzada                     Debian y Ubuntu
    
    echo   3:linux-xanmod-lts  Version con largo soporte tecnico    Debian y Ubuntu
    
    echo   4:linux-liquorix    Version Estable                      Debian 
    
    echo   5:linux-liquorix    Version Estable                      Ubuntu
    
    echo   6:linux-xanmod-rt   Version Estable en tiempo real      Fedora
    
    echo   7:linux-xanmod-edge Version avanzada                     Fedora 
    
    echo   8:linux-xanmod-lts  Version con largo soporte tecnico    Fedora
    echo n: Salir
    echo ""
    sleep 1s
    read -p "Opcion: " sn
    echo
    case $sn in
            [1]* )  echo
                    echo  Ahora actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo apt update -y  && sudo apt upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list &&
                   wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/xanmod-kernel.gpg add -
                    
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sudo apt update && sudo apt install linux-xanmod
                    echo 
                    sleep 3s
                    echo
                    echo  
                    sleep 3s ;;
          
             [2]* ) echo 
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo apt update -y  && sudo apt upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list && 
                   wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/xanmod-kernel.gpg add -

                    sleep 2s
                    echo
                    echo Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo apt update && sudo apt install linux-xanmod-edge
                    sleep 2s ;;
                    
             [3]* ) echo
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo apt update -y  && sudo apt upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list &&
                   wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/xanmod-kernel.gpg add -
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo apt update && sudo apt install linux-xanmod-lts
                    sleep 2s
                    sleep 1s ;;

             [4]* ) echo
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo apt update -y  && sudo apt upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    sudo apt install curl && curl 'https://liquorix.net/add-liquorix-repo.sh' | sudo bash
                    echo
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64
                    sleep 2s
                    sleep 1s ;;

             [5]* ) echo
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo apt update -y  && sudo apt upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    sudo add-apt-repository ppa:damentz/liquorix && sudo apt-get update
                    echo
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64
                    sleep 2s
                    sleep 1s ;;
                    
             [6]* ) echo
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo dnf update -y  && sudo dnf upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    sudo dnf copr enable rmnscnce/kernel-xanmod
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo dnf update && sudo dnf install kernel-xanmod-rt
                    sleep 2s
                    sleep 1s ;;
                    
             [7]* ) echo
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo dnf update -y  && sudo dnf upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    sudo dnf copr enable rmnscnce/kernel-xanmod
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo dnf update && sudo dnf install kernel-xanmod-edge 
                    sleep 2s
                    sleep 1s ;;
     
            [8]*  ) echo
                    echo  Ahora Actualizaremos el sistema, Para evitar problemas.
                    sleep 2s
                    echo
                    echo "";
                    sudo dnf update -y  && sudo dnf upgrade -y
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el repositorio del Kernel, la llave de este etc... 
                    echo 
                    sleep 2s
                    sudo dnf copr enable rmnscnce/kernel-xanmod
                    sleep 2s
                    echo
                    echo  Ahora instalaremos el kernel...
                    echo 
                    sleep 2s
                    sudo dnf update && sudo dnf install kernel-xanmod-lts
                    sleep 2s
                    sleep 1s ;;
                    [Nn]* ) echo " Operación abortada....saliendo....."
		    exit;;
              esac

              echo ""
              echo ""

                    echo "Ya hemos terminado."
                    sleep 2s
                    echo "Ya tienes tu kernel instalado."
                    echo Es scrip tiene como objetivo instalar el kernel xanmod y liquorix de una forma sencilla, este cumple las 4 libertades del software libre.
                    echo
                    echo
                    echo -e "La libertad de ejecutar el software como te plazca y con cualquier objetivo.\nLa libertad de estudiar como funciona el programa y cambiarlo a tu gusto.\nLa libertad de poder redistribuir copias del programa a los demás.\nLa libertad de poder distribuir también tus mejoras al programa original."
                    echo
                    echo
                    echo          
                    sleep 2s

              read -p "¿Quieres reiniciar ahora (s/n)?" sn
              case $sn in
                     [Ss]* )  sudo reboot;;
                     [Nn]* ) exit;;
                     * ) echo "Por favor, pulsa s o n.";;
              esac
       done          
                    
                    
                    
                    
                  
