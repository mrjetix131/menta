# MENTA

Este script te permite instalar el kernel xanmod y liquorix de una forma facil, sencilla y pracica en nuestro linux.

Este script esta pensado para funcionar en Fedora, Debían y Ubuntu en cualquier entorno de escritorio ya sea: Gnome, Kd Plasma e
Xfce.


## PASOS PARA UTILIZAR/INTALAR EL SCRIPT
Debes seguir los pasos a continuación correctamente para ejecutar el script y no tener ningun error en el processo.

Los pasos a seguir son:

```bash
chmod +x menta.sh    #para dar permisos de ejecución al script
```

```bash
sh menta.sh          #Para ejecutar el script
```
